package com.nttdata.bankservice.infrastructure.data.repositories;
import com.nttdata.bankservice.domain.document.Client;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
public interface ClientRepository extends ReactiveMongoRepository<Client,String> {

}
