package com.nttdata.bankservice.domain.interfaz;

import com.nttdata.bankservice.domain.document.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {
    public List<Client> getAll();
    public Optional<Client> getUser(int idClient);
    public Client save(Client client);
    public boolean delete(int idClient);
}
