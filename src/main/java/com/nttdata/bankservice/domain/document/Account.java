package com.nttdata.bankservice.domain.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private String type;
    private String amount;
    @NotEmpty
    private String commission;
    private String clientTipe;
    private String signature;  //firma
}
