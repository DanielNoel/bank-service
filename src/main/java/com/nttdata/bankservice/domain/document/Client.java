package com.nttdata.bankservice.domain.document;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    @Id
    private String id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String surname;
    private String dni;
    private String direction;
    private String email;
    private String phone;

}
