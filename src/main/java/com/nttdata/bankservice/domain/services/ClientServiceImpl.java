package com.nttdata.bankservice.domain.services;

import com.nttdata.bankservice.domain.document.Client;
import com.nttdata.bankservice.domain.interfaz.ClientService;

import java.util.List;
import java.util.Optional;

public class ClientServiceImpl implements ClientService {

    @Override
    public List<Client> getAll() {
        return null;
    }

    @Override
    public Optional<Client> getUser(int idClient) {
        return Optional.empty();
    }

    @Override
    public Client save(Client client) {
        return null;
    }

    @Override
    public boolean delete(int idClient) {
        return false;
    }
}
